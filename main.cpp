/* =====================================================
*             MIST (Infinite Cloud network)
*                    By S@n1X D4rk3r
*             https://github.com/Sanix-Darker/Mist
*======================================================*/
#include <iostream>
#include <fstream>
#include <ctype.h>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <windows.h>
#include <boost/algorithm/string.hpp>

#ifdef WIN32
#include <windows.h>
#else
#include <termios.h>
#include <unistd.h>
#endif

#include "sha1.h"
#include "mist.h"

using namespace std;


int main(){

    string authentificatedvalue="",
            MistID="",
            menuchoice="";
    welcome();
    //The load method here to check in files  for all components and network
    begining:
        if(fexists("MistID")){ // alors le fichier existe
            cout<<"-MistID(if it's not your account, hit 'Enter')";
            //To desactivate spealing
        }else{ // Le fichier n'existe pas
            cout<<"This Host doesn't have a MistID, create your own to use Mist!(more than 7 caracters)";
            create_MistID(MistID,menuchoice,authentificatedvalue);
        }

        SetStdinEcho(false);
        backNewLine(); getline(cin,authentificatedvalue);
        SetStdinEcho(true);
        if(Tolower(authentificatedvalue)=="no" || Tolower(authentificatedvalue)=="n"){ //if the one don't have authentifiacation ID for Mist
            //We verify first if an ID don't exist in a file with the SHA1 Crypt ID (l'ordre de cryptage devra etre aleatoire en fonction de chaque creation de son MistID)
            //To create the mistID
            create_MistID(MistID,menuchoice,authentificatedvalue);

        }else{
            if(read_file("MistID")==SHA512_encrypt(authentificatedvalue)){
                MistID=SHA512_encrypt(authentificatedvalue); // we put in Mist ID the sha crypt value
                BacktoClear(MistID,menuchoice);
            }else{
                cout<<"\n-This Mist_ID does not exist or does not match for this host\n";
                goto begining;
            }
        }
    return 0;
}
